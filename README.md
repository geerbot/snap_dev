# Snap Development

## Overview
Simple snap projects to get familiar with process.

## Instructions
cd hello_go
snapcraft

snap install hello --devmode

hello 'or' snap run hello
